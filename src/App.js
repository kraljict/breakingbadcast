import React, { useState, useEffect } from "react";
import { Switch, Paper } from "@material-ui/core";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import axios from "axios";
import "./App.css";
import Header from "./components/Header";
import CharacterGrid from "./components/CharacterGrid";
import Search from "./components/Search";

const App = () => {
  const [items, setItems] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [query, setQuery] = useState("");
  const [darkMode, setDarkMode] = useState(true);
  const theme = createMuiTheme({
    palette: {
      type: darkMode ? "dark" : "light",
    } 
  });

  useEffect(() => {
    const fetchItems = async () => {
      const result = await axios(
        `https://www.breakingbadapi.com/api/characters?name=${query}`
        );
        setItems(result.data);
        setIsLoading(false);
      };
      
      fetchItems();
    }, [query]);
    
  return (
    <ThemeProvider theme={theme}>
      <Paper style={{ height: "100%" }}>
        <div className="container">
          <Switch
            checked={darkMode}
            onClick={() => setDarkMode(!darkMode)}
            className="toggle"
          />
          <Header />
          <Search getQuery={(q) => setQuery(q)} />
          <CharacterGrid isLoading={isLoading} items={items} />
        </div>
      </Paper>
    </ThemeProvider>
  );
};

export default App;
